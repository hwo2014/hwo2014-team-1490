/*******************************************************************************
 * Hello World Open 2014
 *
 * The Code Racer - Bot
 *
 *
 * Antti Harri
 *
 *
 * /History
 *
 * 2014.04.15 Initial version from the Bitbucket + first commit
 *            "Here I am, that's all I can!"
 *
 *******************************************************************************/

#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "cJSON.h"

#define TEST_MSG_1 "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"indianapolis\",\"name\":\"Indianapolis\",\"pieces\":[{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":200,\"angle\":22.5}],\"lanes\":[{\"distanceFromCenter\":-20,\"index\":0},{\"distanceFromCenter\":0,\"index\":1},{\"distanceFromCenter\":20,\"index\":2}],\"startingPoint\":{\"position\":{\"x\":-340.0,\"y\":-96.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Schumacher\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}},{\"id\":{\"name\":\"Rosberg\",\"color\":\"blue\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":30000,\"quickRace\":true}}}}"

#define TEST_MSG_2 "{\"msgType\":\"yourCar\",\"data\":{\"name\":\"Schumacher\",\"color\":\"red\"}}"

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *make_msg(char *type, cJSON *msg);

static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp( "gameInit", msg_type_name ) ) {
        puts( "Game init received!" );
/*
        msg_data = cJSON_GetObjectItem(msg, "data");
        if( msg_data == NULL )
        {
            puts( "NULL data\n" );
        }
        else
        {
            char* printData;
            int len = 100;
            printData = (char*)malloc( len );
            memcpy( printData, msg_data->valuestring, len );
            printf( "data: %s\n", printData );
            free( printData );
        }
*/
    } else if (!strcmp( "yourCar", msg_type_name ) ) {
        puts( "Your car info received!" );
        msg_data = cJSON_GetObjectItem(msg, "data");
        cJSON *test = cJSON_GetObjectItem( msg_data, "name" );
        if( test == NULL )
        {
            puts( "NULL data\n" );
        }
        else
        {
            char* printData;
            int len = 100;
            printData = (char*)malloc( len );
            memcpy( printData, test->valuestring, len );
            printf( "name: %s\n", printData );
            free( printData );
        }
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp( "lapFinished", msg_type_name) ) {
        puts( "Lap finished" );
    }else if (!strcmp("crash", msg_type_name)) {
        puts("Someone crashed");
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    } else {
        printf( "Unknown: %s\n", msg_type_name );
    }
}

int main(int argc, char *argv[])
{
    FILE *fp;
    fp=fopen( "json.txt", "w");
    if ( !fp )
    {
        error( "Unable to open file!!!" );
    }

    if ( argc == 2 && strcmp( argv[1], "test" ) == 0 )
    {
        printf( "Testing the bot!\n" );
        cJSON *json = cJSON_Parse( TEST_MSG_2 );
        cJSON *msg, *msg_type;
        char *msg_type_name;

        msg_type = cJSON_GetObjectItem(json, "msgType");

        if( msg_type == NULL )
        {
            error("missing msgType field");
        }

        msg_type_name = msg_type->valuestring;

        log_message(msg_type_name, json);
        cJSON_Delete(json);
    }
    else
    {
        int sock;
        cJSON *json;

        if (argc != 5)
            error("Usage: bot host port botname botkey\n");

        sock = connect_to(argv[1], argv[2]);

        json = join_msg(argv[3], argv[4]);
        write_msg(sock, json);
        cJSON_Delete(json);

        while ((json = read_msg(sock)) != NULL) {
            cJSON *msg, *msg_type;
            char *msg_type_name;

            // Just to debug JSON messaging.
            char *print_JSON = cJSON_Print( json );
            fprintf(fp, "%s", print_JSON);
            free( print_JSON );
            fprintf( fp, "\n*********************************************************************************************\n" );

            msg_type = cJSON_GetObjectItem(json, "msgType");
            if (msg_type == NULL)
                error("missing msgType field");

            msg_type_name = msg_type->valuestring;
            if (!strcmp("carPositions", msg_type_name)) {
                msg = throttle_msg(0.6);
            } else {
                log_message(msg_type_name, json);
                msg = ping_msg();
            }

            write_msg(sock, msg);

            cJSON_Delete(msg);
            cJSON_Delete(json);
        }
    }

    fclose( fp );
    return 0;
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}
